# BookStores

<p>Jika terdapat perubahan pada database, silahkan lakukan scaffholding kembali dengan perintah berikut ini:</p>
<ol>
    <li>Scaffold-DbContext 'Name=ConnectionStrings:BookStoresDB' Microsoft.EntityFrameworkCore.SqlServer -OutputDir ../BookStores.Data/Models -Namespace BookStores.Data.Models -Force</li>
</ol>
