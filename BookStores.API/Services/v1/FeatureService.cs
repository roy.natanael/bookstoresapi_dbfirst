﻿using BookStores.API.Interfaces.v1;
using BookStores.Data.Dtos;
using static BookStores.Data.Dtos.ResponseDto;

namespace BookStores.API.Services.v1
{
    public class FeatureService : IFeatureService
    {
        public BaseResponse InsertLog(string errorMsg)
        {
            Guid newGuid = Guid.NewGuid();
            Console.WriteLine(newGuid.ToString());

            /*--------------------------
            Proses Insert Log ke DB
            LogId = newGuid
            LogValue = errorMsg
            --------------------------*/

            return FailedResponse(500, $"Exception Occured | LogId = {newGuid}");
        }
    }
}
