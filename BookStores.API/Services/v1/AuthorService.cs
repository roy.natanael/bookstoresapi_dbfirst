﻿using BookStores.API.Interfaces.v1;
using BookStores.Data.Models;
using BookStores.Data.Interfaces.v1;
using BookStores.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using BookStores.Data.Dtos;
using static BookStores.Data.Dtos.ResponseDto;


namespace BookStores.API.Services.v1
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IFeatureService _featureService;

        public AuthorService(IAuthorRepository authorRepository, IFeatureService featureService)
        {
            _authorRepository = authorRepository;
            _featureService = featureService;
        }

        public async Task<BaseResponse> GetAllAuthors(int page)
        {
            try
            {
                var result = await _authorRepository.Get(page);

                if (result.Count == 0) return FailedResponse(404, "data not found");

                return SuccessResponse(200, result);

            }
            catch (Exception ex)
            {
                return _featureService.InsertLog(ex.Message.ToString());
            }
        }

        public async Task<BaseResponse> TotalAuthorsPage()
        {
            try
            {
                int count = await _authorRepository.Count();
                int total_pages = (int)Math.Ceiling((double)count / 5);

                return SuccessResponse(200, new { total_pages });

            }
            catch (Exception ex)
            {
                return _featureService.InsertLog(ex.Message.ToString());
            }
        }

        public async Task<BaseResponse> GetAuthor(int id)
        {
            try
            {
                var data = await _authorRepository.GetById(id);

                if (data == null) return FailedResponse(404, "data not found");

                return SuccessResponse(200, data);

            }
            catch (Exception ex)
            {
                return _featureService.InsertLog($"{ex.Message}");
            }
        }
    }
}
