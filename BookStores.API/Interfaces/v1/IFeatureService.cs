﻿
using BookStores.Data.Dtos;

namespace BookStores.API.Interfaces.v1
{
    public interface IFeatureService
    {
        BaseResponse InsertLog(string errorMsg);
    }
}
