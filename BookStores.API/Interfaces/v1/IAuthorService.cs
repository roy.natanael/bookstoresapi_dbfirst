﻿
using BookStores.Data.Dtos;

namespace BookStores.API.Interfaces.v1
{
    public interface IAuthorService
    {
        Task<BaseResponse> GetAllAuthors(int page);
        Task<BaseResponse> TotalAuthorsPage();
        Task<BaseResponse> GetAuthor(int id);
    }
}
