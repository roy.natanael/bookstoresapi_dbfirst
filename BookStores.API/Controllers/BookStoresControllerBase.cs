﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using static BookStores.Data.Dtos.ResponseDto;

namespace BookStores.API.Controllers
{
    public class BookStoresControllerBase : ControllerBase
    {
        public override BadRequestObjectResult BadRequest([ActionResultObjectValue] ModelStateDictionary modelState)
        {
            string message = string.Join(" | ", ModelState.Values
                   .SelectMany(v => v.Errors)
                   .Select(e => e.ErrorMessage));

            BadRequestObjectResult objectResult = new BadRequestObjectResult(ModelState)
            {
                Value = FailedResponse(400, message),
                Formatters = new FormatterCollection<IOutputFormatter>(),
                ContentTypes = new MediaTypeCollection()
            };

            return objectResult;
        }
    }
}
