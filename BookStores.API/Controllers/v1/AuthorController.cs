﻿using BookStores.API.Interfaces.v1;
using BookStores.Data.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BookStores.API.Controllers.v1
{
    [Route("api/v1/[Controller]")]
    [ApiController]
    public class AuthorController : BookStoresControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpGet("")]
        public async Task<ActionResult<BaseResponse>> GetAllAuthors([FromQuery] GetAllAuthorsParam param)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var data = await _authorService.GetAllAuthors(int.Parse(param.Page));
            return StatusCode(data.Code, data);
        }

        [HttpGet("totalpages")]
        public async Task<ActionResult<BaseResponse>> TotalAuthorsPage()
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var data = await _authorService.TotalAuthorsPage();
            return StatusCode(data.Code, data);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BaseResponse>> GetAuthor([Required]int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var data = await _authorService.GetAuthor(id);
            return StatusCode(data.Code, data);
        }
    }
}
