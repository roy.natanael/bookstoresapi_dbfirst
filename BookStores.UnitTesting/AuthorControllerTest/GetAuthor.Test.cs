﻿using BookStores.API.Controllers.v1;
using BookStores.API.Interfaces.v1;
using BookStores.API.Services;
using BookStores.API.Services.v1;
using BookStores.Data.Dtos;
using BookStores.Data.Interfaces.v1;
using BookStores.Data.Models;
using BookStores.Data.Repositories;
using BookStores.Data.Repositories.v1;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BookStores.Data.Dtos.ResponseDto;


namespace BookStores.Test.AuthorControllerTest
{
    public class GetAuthor : BaseTestClass
    {
        [Fact]
        public async Task GetAuthor_MockTest_1() // Mock Test, If Author Count = 0
        {
            var authorRepository = new Mock<IAuthorRepository>();
            IFeatureService utilService = new FeatureService();
            IAuthorService authorService = new AuthorService(authorRepository.Object, utilService);

            var controller = new AuthorController(authorService);

            var authorList = new List<Author>();
            authorRepository?.Setup(opt => opt.Get(1)).Returns(Task.FromResult(authorList));


            GetAllAuthorsParam param = new GetAllAuthorsParam()
            {
                Page = "1"
            };

            var response = await controller.GetAllAuthors(param);
            ObjectResult? objectResult = (ObjectResult?)response?.Result;
            var actualResult = (BaseResponse?)objectResult?.Value;

            Assert.False(actualResult?.Status);
            Assert.Null(actualResult?.Data);
        }

        [Fact]
        public async Task GetAuthor_MockTest_2() // Mock Test, If Author Count > 0
        {
            var authorRepository = new Mock<IAuthorRepository>();
            IFeatureService utilService = new FeatureService();

            IAuthorService authorService = new AuthorService(authorRepository.Object, utilService);

            var controller = new AuthorController(authorService);

            List<Author> authorList =
            [
                new Author {
                    AuthorId = 1,
                    LastName="Bennet",
                    FirstName="Abraham",
                    Phone="415 658-9932",
                    Address="Street",
                    City="Berkeley",
                    State="Indonesia",
                    Zip="95688",
                    EmailAddress="test@mail.com",
                    BookAuthors=[]
                }
            ];

            authorRepository?.Setup(opt => opt.Get(1)).Returns(Task.FromResult(authorList));

            GetAllAuthorsParam param = new GetAllAuthorsParam()
            {
                Page = "1"
            };
            var response = await controller.GetAllAuthors(param);
            ObjectResult? objectResult = (ObjectResult?)response?.Result;

            var actualResult = (BaseResponse?)objectResult?.Value;
            var expectedResult = ResponseDto.SuccessResponse(200, authorList);

            Assert.True(actualResult?.Status);
            Assert.Equal(actualResult?.Data, expectedResult?.Data);
        }

        [Fact]
        public async Task GetAuthor_MockTest_3() // Mock Test, If Exception Occured
        {
            var authorRepository = new Mock<IAuthorRepository>();
            IFeatureService utilService = new FeatureService();

            IAuthorService authorService = new AuthorService(authorRepository.Object, utilService);

            var controller = new AuthorController(authorService);

            authorRepository?.Setup(opt => opt.Get(1)).Throws(new Exception());

            GetAllAuthorsParam param = new GetAllAuthorsParam()
            {
                Page = "1"
            };
            var response = await controller.GetAllAuthors(param);
            ObjectResult? objectResult = (ObjectResult?)response?.Result;

            var actualResult = (BaseResponse?)objectResult?.Value;

            Assert.False(actualResult?.Status);
            Assert.Null(actualResult?.Data);
        }

        [Fact]
        public async Task GetAuthor_DBTest() // DB Test, If Author Count > 0
        {
            IAuthorRepository authorRepository = new AuthorRepository(_bookStoresDbContext);
            IFeatureService utilService = new FeatureService();
            IAuthorService authorService = new AuthorService(authorRepository, utilService);

            var controller = new AuthorController(authorService);

            GetAllAuthorsParam param = new GetAllAuthorsParam()
            {
                Page = "1"
            };

            var response = await controller.GetAllAuthors(param);
            ObjectResult? objectResult = (ObjectResult?)response?.Result;
            var actualResult = (BaseResponse?)objectResult?.Value;

            Assert.True(actualResult?.Status);
            Assert.NotNull(actualResult?.Data);
        }
    }
}
