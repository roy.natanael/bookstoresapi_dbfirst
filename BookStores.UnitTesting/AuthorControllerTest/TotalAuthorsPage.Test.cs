﻿using BookStores.API.Controllers.v1;
using BookStores.API.Interfaces.v1;
using BookStores.API.Services;
using BookStores.API.Services.v1;
using BookStores.Data.Dtos;
using BookStores.Data.Interfaces.v1;
using BookStores.Data.Repositories;
using BookStores.Data.Repositories.v1;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BookStores.Test.AuthorControllerTest
{
    public class TotalAuthorsPage : BaseTestClass
    {
        [Fact]
        public async Task TotalAuthorsPage_MockTest_1() // Mock Test
        {
            var authorRepository = new Mock<IAuthorRepository>();
            IFeatureService featureService = new FeatureService();
            IAuthorService iAuthorService = new AuthorService(authorRepository.Object, featureService);
            var controller = new AuthorController(iAuthorService);


            authorRepository.Setup(opt => opt.Count()).Returns(Task.FromResult(22));

            var response = await controller.TotalAuthorsPage();
            ObjectResult? objectResult = (ObjectResult?)response?.Result;
            var actualResult = (BaseResponse?)objectResult?.Value;

            Assert.True(actualResult?.Status);
        }

        [Fact]
        public async Task TotalAuthorsPage_MockTest_2() // Mock Test, IF Exception Occured
        {
            var authorRepository = new Mock<IAuthorRepository>();
            IFeatureService featureService = new FeatureService();
            IAuthorService iAuthorService = new AuthorService(authorRepository.Object, featureService);
            var controller = new AuthorController(iAuthorService);

            authorRepository.Setup(opt => opt.Count()).Throws(new Exception());

            var response = await controller.TotalAuthorsPage();
            ObjectResult? objectResult = (ObjectResult?)response?.Result;

            var actualResult = (BaseResponse?)objectResult?.Value;

            Assert.False(actualResult?.Status);
            Assert.Null(actualResult?.Data);
        }

        [Fact]
        public async Task TotalAuthorsPage_DBTest() // DB Test
        {
            IAuthorRepository authorRepository = new AuthorRepository(_bookStoresDbContext);
            IFeatureService featureService = new FeatureService();
            IAuthorService authorService = new AuthorService(authorRepository, featureService);

            var controller = new AuthorController(authorService);

            var response = await controller.TotalAuthorsPage();
            ObjectResult? objectResult = (ObjectResult?)response?.Result;

            var actualResult = (BaseResponse?)objectResult?.Value;

            Assert.True(actualResult?.Status);
            Assert.NotNull(actualResult?.Data);
        }
    }
}
