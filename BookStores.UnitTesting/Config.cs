﻿using BookStores.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStores.Test
{
    public class Config
    {
        public static string GetConnectionString(string configName)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(AppContext.BaseDirectory)
              .AddJsonFile("appsettings.json")
              .Build();

            return configuration[$"ConnectionStrings:{configName}"] ?? "";
        }
    }

    public class BookStoresDbContextConfig(DbContextOptions<BookStoresDbContext> options) : BookStoresDbContext(options)
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Config.GetConnectionString("BookStoresDB"));
        }
    }

    public class BaseTestClass
    {
        public readonly BookStoresDbContextConfig _bookStoresDbContext;

        public BaseTestClass()
        {
            var bookStoresOptions = new DbContextOptions<BookStoresDbContext>();
            _bookStoresDbContext = new BookStoresDbContextConfig(bookStoresOptions);
        }
    }
}
