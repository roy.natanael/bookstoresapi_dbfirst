﻿
namespace BookStores.Data.Dtos
{

    public class BaseResponse
    {
        public bool? Status { get; set; }
        public int Code { get; set; }
        public string? Message { get; set; }
        public dynamic? Data { get; set; }
    }

    public static class ResponseDto
    {
        public static BaseResponse SuccessResponse(int code, dynamic data)
        {
            return new BaseResponse
            {
                Code = code,
                Status = true,
                Message = "OK",
                Data = data,
            };
        }

        public static BaseResponse FailedResponse(int code, string msg)
        {
            return new BaseResponse
            {
                Code = code,
                Status = false,
                Message = msg,
                Data = null,
            };
        }
    }
}
