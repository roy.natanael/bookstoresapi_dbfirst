﻿using System.ComponentModel.DataAnnotations;

namespace BookStores.Data.Dtos
{
    public class GetAllAuthorsParam
    {
        [Required]
        [Range(1, 100)]
        public string Page { get; set; } = string.Empty;
    }

    public class AddAuthorsParam
    {
        public string LastName { get; set; } = null!;

        public string FirstName { get; set; } = null!;

        public string Phone { get; set; } = null!;

        public string? Address { get; set; }

        public string? City { get; set; }

        public string? State { get; set; }

        public string? Zip { get; set; }

        public string? EmailAddress { get; set; }

        public string? Hobby { get; set; }

    }
}
