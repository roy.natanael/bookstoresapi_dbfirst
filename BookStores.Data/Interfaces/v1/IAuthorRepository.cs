﻿using BookStores.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStores.Data.Interfaces.v1
{
    public interface IAuthorRepository
    {
        Task<List<Author>> Get(int page);
        Task<int> Count();
        Task<Author?> GetById(int id);
    }
}
