﻿using BookStores.Data.Interfaces.v1;
using BookStores.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStores.Data.Repositories.v1
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly BookStoresDbContext _dbContext;

        public AuthorRepository(BookStoresDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> Count()
        {
            var count = await _dbContext.Authors.CountAsync();
            return count;
        }

        public async Task<List<Author>> Get(int page)
        {
            var result = await _dbContext.Authors
                    .Skip((page - 1) * 5)
                    .Take(5)
                    .ToListAsync();

            return result;
        }

        public async Task<Author?> GetById(int id)
        {
            var author = await _dbContext.Authors
                .Where(auth => auth.AuthorId == id)
                .FirstOrDefaultAsync();

            return author;
        }
    }
}
